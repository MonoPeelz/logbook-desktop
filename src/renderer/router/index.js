import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/installation',
      name: 'installation',
      component: require('@/components/Installation').default
    },
    {
      path: '/login',
      name: 'login',
      component: require('@/components/LoginPage').default

    },

    {
      path: '/edit-profile',
      name: 'edit-profile',
      component: require('@/components/EditProfilePage').default

    },

    {
      path: '/5year',
      name: '5-year',
      component: require('@/components/FiveYearAssessmentPage').default
    },

    {
      path: '/summary',
      name: 'summary',
      component: require('@/components/SummaryReport').default
    },

    {
      path: '/plc',
      name: 'plc.index',
      component: require('@/components/Contents/Plc/ListPage').default
    },
    {
      path: '/plc/create',
      name: 'plc.create',
      component: require('@/components/Contents/Plc/CreatePage').default
    },
    {
      path: '/plc/edit/:id',
      name: 'plc.edit',
      component: require('@/components/Contents/Plc/EditPage').default
    },
    // 
    {
      path: '/development',
      name: 'development.index',
      component: require('@/components/Contents/Development/ListPage').default
    },
    {
      path: '/development/create',
      name: 'development.create',
      component: require('@/components/Contents/Development/CreatePage').default
    },
    {
      path: '/development/edit/:id',
      name: 'development.edit',
      component: require('@/components/Contents/Development/EditPage').default
    },
    //
    {
      path: '/assignment',
      name: 'assignment.index',
      component: require('@/components/Contents/Assignment/ListPage').default
    },
    {
      path: '/assignment/create',
      name: 'assignment.create',
      component: require('@/components/Contents/Assignment/CreatePage').default
    },
    {
      path: '/assignment/edit/:id',
      name: 'assignment.edit',
      component: require('@/components/Contents/Assignment/EditPage').default
    },
    // 
    {
      path: '/responsive',
      name: 'responsive.index',
      component: require('@/components/Contents/Responsive/ListPage').default
    },
    {
      path: '/responsive/create',
      name: 'responsive.create',
      component: require('@/components/Contents/Responsive/CreatePage').default
    },
    {
      path: '/responsive/edit/:id',
      name: 'responsive.edit',
      component: require('@/components/Contents/Responsive/EditPage').default
    },
      // 
    {
      path: '/learning-support',
      name: 'learning-support.index',
      component: require('@/components/Contents/LearningSupport/ListPage').default
    },
    {
      path: '/learning-support/create',
      name: 'learning-support.create',
      component: require('@/components/Contents/LearningSupport/CreatePage').default
    },
    {
      path: '/learning-support/edit/:id',
      name: 'learning-support.edit',
      component: require('@/components/Contents/LearningSupport/EditPage').default
    },
    // 
    {
      path: '/timetable',
      name: 'timetable.index',
      component: require('@/components/Contents/TimeTable/ListPage').default
    },
    {
      path: '/timetable/create',
      name: 'timetable.create',
      component: require('@/components/Contents/TimeTable/CreatePage').default
    },
    {
      path: '/timetable/edit/:id',
      name: 'timetablet.edit',
      component: require('@/components/Contents/TimeTable/EditPage').default
    },
    // 
    {
      path: '/academic',
      name: 'academic.index',
      component: require('@/components/Contents/AcademicStanding/ListPage').default
    },
    {
      path: '/academic/edit',
      name: 'academic.edit',
      component: require('@/components/Contents/AcademicStanding/EditPage').default
    },
    // 
    {
      path: '/home',
      name: 'home',
      component: require('@/components/HomePage').default
    },
    {
      path: '/landing-page',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/',
      redirect: '/landing-page'
    }
  ]
})
