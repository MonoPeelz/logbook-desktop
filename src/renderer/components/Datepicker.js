
import Datepicker from 'vuejs-datepicker';
import {en, es} from 'vuejs-datepicker/dist/locale'
import {th} from 'vuejs-datepicker/dist/locale'

export default {
  extends: Datepicker,
  props: {
    language:{
      default(){return th} 
    }
  },
  mounted() {

  },
  data() {
    return {
      // language: th
    }
  }
}
