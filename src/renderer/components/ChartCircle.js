
import { Doughnut, mixins } from 'vue-chartjs'

export default {
  extends: Doughnut,
  mixins: [mixins.reactiveProp],
  props: ['data','chartData' , 'options'  ],
  // props: {
  //   'height':{default: 200}
  // },
  data() {
    return {
      style:{
        margin: 'auto'
      }
      // width: 200,
      // height: 200  
    }
  },
  mounted () {
    this.renderChart(this.data, this.options)

    // Overwriting base render method with actual data.
    // this.renderChart({
    //   labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    //   datasets: [
    //     {
    //       label: 'GitHub Commits',
    //       backgroundColor: '#f87979',
    //       data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11]
    //     }
    //   ]
    // })
  }
}
