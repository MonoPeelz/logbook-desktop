const state = {
    showNav: false,
    currentPath: '',
    auth: false,
    install: false,
    exceptShowNavPath: [
      "/login",
      "/installation",
      "/landing-page"  
    ]
}
  
  const mutations = {
    TOGGLE_NAV (state) {
      state.showNav = ! state.showNav
    },
    set_nav(state, show){
      state.showNav = show;
    },
    set_auth(state, auth) {
      state.auth = auth;
    },
    set_install(state, install) {
      state.install = install;
    }
   
  }
  
  const actions = {
    toggleNav ({ commit }) {
      commit('TOGGLE_NAV')
    }
  }
  export default {
    state,
    mutations,
    actions
  }