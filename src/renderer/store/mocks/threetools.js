const data = [
  [
    {
      title: "สร้างและพัฒนาหลักสูตร"
    },
    {
      title: "การจัดการเรียนรู้",
      subs: [
        {
          title: "การออกแบบหน่วยการเรียนรู้"
        },
        {
          title: "การจัดทำแผนการจัดการเรียนรู้/แผนการจัดการศึกษาเฉพาะบุคคล (IEP)/แผนการสอนเฉพาะบุคคล (IIP)/แผนการจัดประสบการณ์"
        },
        {
          title: "กลยุทธ์ในการจัดการเรียนรู้"
        },
        {
          title: "คุณภาพผู้เรียน"
        },
      ]
    },
    {
      title: "การสร้างและการพัฒนาสื่อนวัตกรรมเทคโนโลยีทางการศึกษา และแหล่งเรียนรู้ในการจัดการเรียนรู้"
    },
    {
      title: "การวัดและประเมินผลการเรียนรู้"
    },
    {
      title: "การวิจัยเพื่อพัฒนาการเรียนรู้"
    }
  ],
  [
    {
      title:"การบริหารจัดการชั้นเรียน"
    },
    {
      title:"การจัดระบบดูแลช่วยเหลือผู้เรียน"
    },
    {
      title:"การจัดทำข้อมูลสารสนเทศ และเอกสารประจำชั้นเรียนหรือประจำวิชา"
    },
  ],
  [
    {
      title:"การพัฒนาตนเอง"

    },
    {
      title:"การพัฒนาวิชาชีพ"

    }
  ]
]
  
  export default data ;
  
  