const teacher = [
    {
      first_name: "ศิริพร",
      last_name: "สมประสงค์",
      birth_day: "20",
      birth_month: "3",
      birth_year: "2527",
      gendar: "ชาย",
      tel_no: "0872232223",
      teacher_id: "1111111",
      citizen_id: "1234567891011",
      email: "sompasong@mail.com",
      password: "123456",
      // [
      //   "ครูผู้ช่วย",
      //   "ครู คศ. 1", 
      //   "ครู คศ. 2", 
      //   "ครู คศ. 3", 
      //   "ครู คศ. 4", 
      //   "ครู คศ. 5", 
      // ]
      academic_standing: "ครูผู้ช่วย",
      academic_wishes: "ครู คศ. 1",
      school_name: "สมประสงค์วิทยา",
      region: "เขต 4",
      role: "",
      education: "ปริญญาตรี",
      role_id: "25",
      subject_group: "วิทยาศาสตร์",
      // จน.วิชาที่สอน
      courses_taught: "5",
      salary: "19000",
      // ระดับชั้น
      level: "",
    }
  ]
  
  export default 
    teacher
   ;