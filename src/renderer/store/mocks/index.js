const files = require.context('.', false, /\.js$/)
const mocks = {}

files.keys().forEach(key => {
  if (key === './index.js') return
  mocks[key.replace(/(\.\/|\.js)/g, '')] = files(key).default
})

export default mocks
// console.log(require("../mocks"));

