import Vue from 'vue'
import axios from 'axios'

import LeftSidenav from './components/Layouts/LeftSidenav'
import router from './router'
import store from './store'
import storage from './storage'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserCog, faInfoCircle, faPlayCircle, faSignOutAlt, faChartLine, faChartPie, faTimes, faPlus, faEdit, faTrash, faCalendarAlt, faHome } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import HeaderContent from "@/components/Layouts/HeaderContent"
import Datepicker from './components/Datepicker'

library.add( faUserCog, faInfoCircle, faPlayCircle, faSignOutAlt, faChartLine, faChartPie, faTimes, faPlus, faEdit, faTrash, faCalendarAlt, faHome)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('header-content', HeaderContent)
Vue.component('datepicker', Datepicker)
Vue.config.productionTip = false
if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
import App from './App'
// import Auth from './Auth'
Vue.config.devtools = true;


var auth = true ;
// if already auth
new Vue({
  components: { App },
  router,
  storage,
  store,
  template: '<App/>'
}).$mount('#app')
// if ( auth ) {
//   new Vue({
//     components: { App },
//     router,
//     storage,
//     store,
//     template: '<App/>'
//   }).$mount('#app')
// } else {
//   new Vue({
//     components: { Auth },
//     router,
//     storage,
//     store,
//     template: '<Auth/>'
//   }).$mount('#app')
// }
/* eslint-disable no-new */

